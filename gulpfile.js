var gulp = require('gulp');
var browser = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');

var root = './public/';

gulp.task('server', function() {
    browser({
        server: {
            baseDir:'./public/'
        }
    });
});

gulp.task('html', function () {
    gulp.src(root + '/**/*.html')
        .pipe(browser.reload({stream:true}));
});

gulp.task('sass', function () {
    gulp.src(root + '/assets/scss/**.scss')
        .pipe(sass({
            outputStyle: 'expanded',
            indentWidth: '4'
        }).on('error', sass.logError))
        .pipe(plumber())
        .pipe(autoprefixer({
            browsers: ['last 2 versions','Android >= 4.1.0','iOS >= 7'],
            cascade: false,
            grid: true
        }))
        .pipe(gulp.dest(root + '/assets/css/'))
        .pipe(browser.reload({stream:true}));
});

gulp.task('js', function () {
    gulp.src(root + '/assets/js/**/**.js')
        .pipe(browser.reload({stream:true}));
});


gulp.task('default', ['html','sass','js','server'], function () {
    gulp.watch(root + '/assets/**/**.scss', ['sass']);
    gulp.watch(root + '/**/*.html', ['html']);
    gulp.watch(root + '/assets/js/**/**.js', ['js']);
});
