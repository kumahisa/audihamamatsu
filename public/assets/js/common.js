//リサイズでリロード
$(function(){
    var timer = false;
    var prewidth = $(window).width();
    $(window).resize(function() {
        if (timer !== false) {
            clearTimeout(timer);
        }
        timer = setTimeout(function() {
            var nowWidth = $(window).width();
            if(prewidth !== nowWidth){
                location.reload();
            }
            prewidth = nowWidth;
        }, 200);
    });
});

//スクロール
$(function(){
    $('a[href*="#"]').on('click',function() {
        var target = $(this).attr('href');
        var position = $(target).offset().top;

        $('body,html').animate({scrollTop:position}, 400, 'swing');
        return false;
    });
});


$(function(){
    var s = $('.top .header');
    $(window).scroll(function () {
        if ( $(this).scrollTop() > 500 ) {
            s.addClass('isScrolled');
        } else {
            s.removeClass('isScrolled');
        }
    });
});

$(function(){
    var s = $('.sub .header');
    $(window).scroll(function () {
        if ( $(this).scrollTop() > 100 ) {
            s.addClass('isScrolled');
        } else {
            s.removeClass('isScrolled');
        }
    });
});

$(function(){
    var s = $('.topBtn');
    $(window).scroll(function () {
        if ( $(this).scrollTop() > 500 ) {
            s.addClass('isScrolled');
        } else {
            s.removeClass('isScrolled');
        }
    });
});


//----------------------------------------------
//ヘッダー
//----------------------------------------------

//アコーディオンメニュー
if (matchMedia) {
  const mq = window.matchMedia("(min-width: 721px)");
  mq.addListener(WidthChange);
  WidthChange(mq);
}
function WidthChange(mq) {
  if (mq.matches) {
        $(function() {
          $(".headMenu .navList__item").hover(function(){
              $(this).toggleClass("open");
          });
        });
  } else {
      $(function(){
        $('.headMenu .navList__item').click(function(e) {
            $(this).children('.childList').slideToggle('fast');
            e.stopPropagation();
            $(this).toggleClass("open");
        });
      });
  }
}

//ハンバーガーメニュー
$(function(){
    $('.humburgerBtn').on('click',function(){
        $('.humburgerBtn .humburgerLine').toggleClass("open");
        $(this).toggleClass("open");
        $('nav').slideToggle('fast');
    });
});
